import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt

from pox.core import core
from pox.lib.revent import EventMixin
from pox.openflow import PacketIn, ConnectionUp

log = core.getLogger()


class FirewalledLearningSwitch(EventMixin):
    def __init__(self, connection):
        self.mac_table = {}
        self.open_secure_conns = {}

        self.connection = connection
        self.connection.addListeners(self)

        self.idle_timeout = 10
        self.hard_timeout = 30
        self.mac_clear_timeout = 60

    def clear_mac_table(self):
        self.mac_table.clear()

    def schedule_clear_mac_table(self):
        self.clear_mac_table()
        core.callDelayed(self.mac_clear_timeout, self.schedule_clear_mac_table)

    def _handle_PacketIn(self, event):
        packet = event.parsed

        def flood():
            msg = of.ofp_packet_out()
            msg.actions.append(of.ofp_action_output(port=of.OFPP_FLOOD))
            msg.data = event.data
            msg.in_port = event.port
            self.connection.send(msg)

        def drop():
            msg = of.ofp_flow_mod()
            msg.match = of.ofp_match.from_packet(packet)
            msg.idle_timeout = self.idle_timeout
            msg.hard_timeout = self.hard_timeout
            msg.buffer_id = event.ofp.buffer_id
            self.connection.send(msg)

        def accept():
            msg = of.ofp_flow_mod()
            msg.match = of.ofp_match.from_packet(packet)
            msg.idle_timeout = self.idle_timeout
            msg.hard_timeout = self.hard_timeout
            msg.buffer_id = event.ofp.buffer_id
            msg.data = event.ofp

            if packet.dst not in self.mac_table:
                flood()
                return

            port = self.mac_table[packet.dst]
            if port == event.port:
                drop()

            msg.actions.append(of.ofp_action_output(port=port))

        # Always accept ARP.
        if packet.type == packet.ARP_TYPE:
            flood()
            return

        # Open a secure connection if this is a connection to port 80.
        match = of.ofp_match.from_packet(packet)
        self.open_secure_conns.setdefault(packet.src, [])

        # Check if this is an IP packet with TCP
        if match.dl_type == 0x800 and match.nw_proto == 0x6:
            # Port 6000 opens the port.
            if match.tp_dst == 6000:
                self.open_secure_conns[packet.src].append(packet.dst)
                log.info("%s opened a secure connection." % (packet.src))
            # 7000 closes it.
            elif match.tp_dst == 7000:
                self.open_secure_conns[packet.src].remove(packet.dst)
                log.info("%s closed the secure connection." % (packet.dst))

        # Check if the destination is an allowed destination.
        allowed_destinations = self.open_secure_conns[packet.src]
        if packet.dst in allowed_destinations:
            accept()
        else:
            drop()


class FirewallController(EventMixin):
    def __init__(self, transparent):
        self.listenTo(core.openflow)
        self.transparent = transparent
        core.openflow.addListeners(self, "all")

    def _handle_ConnectionUp(self, event):
        # Delete all mods
        event.connection.send(of.ofp_flow_mod(command=of.OFPFC_DELETE))

        # Create a learning firewalled switch from the connection.
        FirewalledLearningSwitch(event.connection)


def launch(transparent=False):
    core.registerNew(FirewallController, transparent)
