FROM ubuntu:latest

RUN apt-get -y update && apt-get -y install \
  python2.7 \
  python2.7-pip \
  python2.7-wheel \
  git

RUN mkdir -p /opt/app
WORKDIR /opt/app
COPY . .

RUN git submodule update --init --recursive

# Configure STS dependencies.
RUN /opt/app/tools/install_hassel_python.sh
RUN pip2 install pytrie paramiko readline psutil mock

ENTRYPOINT ["python2.7", "/opt/app/simulator.py"]
