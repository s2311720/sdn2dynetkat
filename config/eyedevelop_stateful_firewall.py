
from config.experiment_config_lib import ControllerConfig
from sts.topology import StarTopology
from sts.control_flow.interactive import Interactive
from sts.input_traces.input_logger import InputLogger
from sts.simulation_state import SimulationConfig
from sts.happensbefore.hb_logger import HappensBeforeLogger

# Use POX as our controller
start_cmd = ('''./pox.py '''
             '''openflow.of_01 --address=__address__ --port=__port__ '''
             '''eyedevelop.stateful_firewall''')
controllers = [ControllerConfig(start_cmd, cwd="pox/", address="0.0.0.0")]

topology_class = StarTopology
topology_params = "num_hosts=2"

results_dir = "experiments/eyedevelop_stateful_firewall"

simulation_config = SimulationConfig(controller_configs=controllers,
                                     topology_class=topology_class,
                                     topology_params=topology_params,
                                     hb_logger_class=HappensBeforeLogger,
                                     hb_logger_params=results_dir)

control_flow = Interactive(simulation_config,
                           input_logger=InputLogger())
