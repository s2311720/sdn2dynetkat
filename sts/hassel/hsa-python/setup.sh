#!/bin/bash
if [ "$0" != 'bash' -a "$0" != '/bin/bash' ];then
echo "Please source this script to set PYTHONPATH. $0"
else
a=$(pwd 2>&1)
echo "Add $a to PYTHONPATH."
PYTHONPATH="$PYTHONPATH:$a"
export PYTHONPATH
fi
cd c-bytearray
python2.7 setup.py build
cp build/lib.*/c_wildcard.so ../utils/.
rm -rf build
cd ..

